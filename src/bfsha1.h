/*
 *  bfsha1.h
 *  e412a4-0
 *
 */

#ifndef BFSHA1_H
#define BFSHA1_H

#include <string>

class BFsha1 {
	
private:
	std::string salt;
	std::string target;
	std::string password;
	std::string chars;
	int plength;
	int* pmap;
	bool success;
	
	void makePassword();
	bool increment();
	bool compareToHash();
	void build();
	
public:
	
	BFsha1(std::string salt, std::string hash, std::string chars, int pwdlength);
	BFsha1(std::string hash, std::string chars, int pwdlength);
	BFsha1();
	
	std::string getPassword();
	
	~BFsha1();
	

};

#endif

