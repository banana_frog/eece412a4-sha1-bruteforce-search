#include <iostream>
#include "bfsha1.h"

int main (int argc, char * const argv[]) {
	
	if(argc < 4 || argc > 5) {
		std::cout << "Usage: [program name] [hash] [acceptable characters] [password length] [salt] \n";
		return 0;
	}
	
	BFsha1 s;
	
	if(argc == 5) {
		s = BFsha1(argv[4], argv[1], argv[2], atoi(argv[3]));
		std::cout << "salt: |" << argv[4] << "|\n";
	} else 
		s = BFsha1(argv[1], argv[2], atoi(argv[3]));
	
    std::cout << "target hash: |" << argv[1] << "|\ncharacters: |" << argv[2] <<
				"|\npassword: |" + s.getPassword() + "|\n";
    return 0;
}
