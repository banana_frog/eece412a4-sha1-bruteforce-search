/*
 *  bfsha1.cpp
 *  e412a4-0
 *
 */

#include "bfsha1.h"
#include "sha1.h"
#include <strings.h>

#include <iostream>

void BFsha1::build() {
	success = true;
	pmap = new int[plength];
	std::fill_n(pmap, plength, 0);
	
	while(!compareToHash()) {
		if(!increment()) {
			success = false;
			break;
		}
	}
	
	if(pmap != NULL) {
		free(pmap);
		pmap = NULL;
	}
}


BFsha1::BFsha1(std::string salt, std::string hash, std::string chars, int pwdlength):
	salt(salt), target(hash), chars(chars), plength(pwdlength) { 
		build();
	}

BFsha1::BFsha1(std::string hash, std::string chars, int pwdlength):
	target(hash), chars(chars), plength(pwdlength) {
		salt = "";
		build();
}

BFsha1::BFsha1() {
	success = false;
}

std::string BFsha1::getPassword() {
	if(!success) 
		return "not found"; 
	return password;
}

void BFsha1::makePassword() {
	password = "";
	for(int i=0; i<plength; i++) {
		password += chars[pmap[i]];
	}
}	


bool BFsha1::increment() {
	int i = 0;
	
	while(i < plength && pmap[i] == chars.length()) {
		pmap[i] = 0;
		i++;
	}
	
	if(i == plength) return false;
	
	pmap[i] = pmap[i] + 1;
	return true;
}
	
bool BFsha1::compareToHash() {
	makePassword();
	std::string toHash = salt + password;
	
	unsigned char temphash[20];
	char hexstring[41];
	sha1::calc(toHash.c_str(),toHash.length(), temphash); 
	sha1::toHexString(temphash, hexstring);
	
	return strncasecmp(hexstring, target.c_str(), target.length()) == 0;
}

BFsha1::~BFsha1() {
	if(pmap != NULL) {
		free(pmap);
		pmap = NULL;
	}
}


